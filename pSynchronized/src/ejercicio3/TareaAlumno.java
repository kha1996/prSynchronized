package ejercicio3;

import java.sql.Time;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TareaAlumno implements Runnable {

	private Lavabo vater = null;
	private int max = 5;
	private int min = 5;

	public TareaAlumno(Lavabo vater) {
		this.vater = vater;
	}

	@Override
	public void run() {
		Random miRandom = new Random();
		boolean utilitzat = false;
		while (!utilitzat) {
			int numLavabo = Lavabo.ocupar();
			if (numLavabo != -1) {
				System.out.printf("%s ha ocupat el vater %d.\n", Thread.currentThread().getName(), numLavabo);
				try {
					int duration = miRandom.nextInt(max - min + 1) + min;
					TimeUnit.MILLISECONDS.sleep(duration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				this.vater.liberar(numLavabo);
				System.out.printf("%s ha desocupat el vater %d.\n", Thread.currentThread().getName(), numLavabo);
				utilitzat=true;
			} else {
				try {
					System.out.printf("%s esta esperant el seu torn %d.\n", Thread.currentThread().getName(),
							numLavabo);
					TimeUnit.MILLISECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
