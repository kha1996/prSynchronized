package ejercicio3;

public class Ejercicio3 {
	public static final int NUM_THREADS=10;
	public static void main(String[] args) {

		boolean[] vater = new boolean[5];
		Lavabo lavabo=new Lavabo(vater);
		TareaAlumno[] misAlumno=new TareaAlumno[NUM_THREADS];
		Thread[]misThreads=new Thread[NUM_THREADS];
		
		
		for (int i = 0; i < NUM_THREADS; ++i){
			misAlumno[i]=new TareaAlumno(lavabo);
			misThreads[i] = new Thread(misAlumno[i], "ALUMNE"+i);
		}
		for (int i = 0; i<misThreads.length ; ++i) {
			misThreads[i].start();
		}

	
	}

}
