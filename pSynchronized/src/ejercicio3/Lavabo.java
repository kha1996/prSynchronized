package ejercicio3;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Lavabo {
	static boolean[] vaterOcupats;

	public Lavabo(boolean[] vater) {
		this.vaterOcupats =vater;
		Arrays.fill(vaterOcupats, false);
	}

	synchronized public static int ocupar() {

		int result = -1;
		for (int i = 0; i < vaterOcupats.length; i++) {
			if (vaterOcupats[i] == false) {
				result = i;
				vaterOcupats[i]=true;
				break;
			}
		}
		return result;
	}

	synchronized public void liberar(int i) {
		vaterOcupats[i] = false;
	}

	public int getNumbers() {
		return vaterOcupats.length;
	}

}