package ejercicio1;



/**
 * PROGRAMACION CONCURRENTE 
 * ------------------------
 * 
 * Programación concurrente:  crear programas que ejecutan varias tareas simultaneamente,
 * teniendo en cuenta:
 * - la correcta secuencia de interacciones o comunicaciones entre las tareas (las tareas se hacen
 * en un orden determinado)
 * - el acceso coordinado a los recursos compartidos por las tareas.
 * 
 * PROBLEMA: Cuando varios Threads leen o escriben sobre los mismos datos simultáneamente,
 * acceden a los mismos ficheros o a los mismos registros de una base de datos, se pueden
 * provocar errores o inconsistencias en esos datos. 
 * 
 * EJEMPLO
 * --------
 * En este ejemplo se crean dos Threads accediendo a un mismo objecto que simula una cuenta bancaria
 * con una cantidad inicial de 1000 unidades.
 * 
 * El primer Thread ejecuta una tarea que añade cien veces 1000 unidades a la cuenta y el segundo 
 * ejecuta una tarea que saca cien veces 1000 unidades.
 * 
 * El resultado final debería ser igual a la cantidad inicial 1000 (añadimos tanto como quitamos) sin embargo
 * el resultado es diferente.
 * 
 * El resultado final es erróneo ya que al introducir un sleep() de 10 milisegundos entre obtener el valor almacenado 
 * y asignar un nuevo valor (añadiendo o quitando 1000), los cambios que hagan otros 
 * Threads en ese tiempo de espera seran sobrescritos y por tanto perdidos.
 *
 */
public class Main {

	public static final int SALDO_INICIAL = 1000;

	public static void main(String[] args) {
		// Creamos una cuenta
		Cuenta	miCuenta =new Cuenta();
		// Asignamos como saldo inicial 1000
		miCuenta.setSaldo(SALDO_INICIAL);
		

		//Creamos una tarea que ingresa dinero en la cuenta
		//y la ejecutamos mediante un thread
		//Le pasamos como parámetro de entrada la cuenta creada.
		TareaSumar	miTareaRestar =new TareaSumar(miCuenta);
		Thread miThreadRestar =new Thread(miTareaRestar);
		
		
		//Creamos una tarea que saca dinero de la cuenta
		//y la ejecutamos mediante un thread
		//Le pasamos como parámetro de entrada la cuenta creada.
		TareaRestar miTareaSumar =new TareaRestar(miCuenta);
		Thread miThreadSumar =new Thread(miTareaSumar);
		
		// Mostramos el valor inicial del saldo
		System.out.printf("Thread principal : Saldo Inicial: %d\n", miCuenta.getSaldo());
		
		// Starts the Threads.
		//Ambos estan trabajando sobre el mismo objeto miCuenta
		 miThreadRestar.start();
		miThreadSumar.start();

		try {
			// Esperamos que los dos threads finalicen
			miThreadRestar.join();
			miThreadSumar.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Mostramos el valor final del saldo, que si todo funcionara correctamente
		//debería ser igual al saldo inicial.
		System.out.printf("Thread principal : Saldo Final: %d\n", miCuenta.getSaldo());
	}
}
