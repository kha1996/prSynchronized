package ejercicio1;

import java.util.concurrent.TimeUnit;

public class Cuenta {
	private int saldo;


	public int getSaldo() {
		return saldo;
	}
	
	synchronized public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	synchronized public void rsun() {
		int tmp = getSaldo() - 1000;
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setSaldo(tmp);
	}
	synchronized public void ssun() {
		int tmp = getSaldo() + 1000;
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setSaldo(tmp);
	}

}
