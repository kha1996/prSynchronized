package ejercicio2;

import java.util.concurrent.TimeUnit;

public class ControlEntrada {
	private int contador;
	public int getContador() {
		return contador;
	}
	synchronized public void setContador(int contador) {
		this.contador = contador;
	}
	synchronized public void rsun() {
		int tmp = getContador() - 1;
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setContador(tmp);
	}
	synchronized public void ssun() {
		int tmp = getContador() + 1;
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setContador(tmp);
	}

}