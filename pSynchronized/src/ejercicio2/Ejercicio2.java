package ejercicio2;

import ejercicio1.TareaSumar;

public class Ejercicio2 {

	public static void main(String[] args) {
		ControlEntrada	control =new ControlEntrada();
		
		TareaGuardia1	tareaGuardia1 =new TareaGuardia1(control);
		Thread threadGuardia1 =new Thread(tareaGuardia1);
		
		
		TareaGuardia2	tareaGuardia2 =new TareaGuardia2(control);
		Thread threadGuardia2 =new Thread(tareaGuardia2);
		
		
		TareaGuardia3	tareaGuardia3 =new TareaGuardia3(control);
		Thread threadGuardia3 =new Thread(tareaGuardia3);
		
		
		
		System.out.printf("hay : %d personas \n", control.getContador());
		threadGuardia1.start();
		threadGuardia2.start();
		threadGuardia3.start();
		
		
		try {
			// Esperamos que los dos threads finalicen
			threadGuardia1.join();
			threadGuardia2.join();
			threadGuardia3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("hay : %d personas \n", control.getContador());
		
	}

}
